PY?=python3
PELICAN?=pelican
PELICANOPTS=
# python 2
# SERVERMODULE=SimpleHTTPServer
# python 3
SERVERMODULE=http.server

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

FTP_HOST=localhost
FTP_USER=anonymous
FTP_TARGET_DIR=/

SSH_HOST=localhost
SSH_PORT=22
SSH_USER=root
SSH_TARGET_DIR=$(HOME)/useful/web

S3_BUCKET=my_s3_bucket

CLOUDFILES_USERNAME=my_rackspace_username
CLOUDFILES_API_KEY=my_rackspace_api_key
CLOUDFILES_CONTAINER=my_cloudfiles_container

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

help:
	@echo 'Makefile for a pelican Web site'
	@echo ''
	@echo 'Usage:'
	@echo '   make html                  (re)generate the web site   '
	@echo '   make clean                 remove the generated files  '
	@echo '   make monitor               Monitoring directory for changes.'
	@echo '   make publish               generate using production settings'
	@echo '   make serve                 serve site at http://localhost:8000'
	@echo '   make rsync_upload          upload the web site via rsync     '
	@echo '                                                                '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html'
	@echo '                                                                '

clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)

html:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

monitor:
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
	@echo 'server url: http://0.0.0.0:8000/'
	cd $(OUTPUTDIR) && python3 -m $(SERVERMODULE)

publish:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)

rsync_upload: publish
	rsync -P -rvzc --delete $(OUTPUTDIR)/ servwebuser.legi.grenoble-inp.fr:$(SSH_TARGET_DIR) --cvs-exclude


.PHONY: help clean monitor serve publish rsync_upload
