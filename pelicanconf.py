#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Les gentils organisateurs'
SITENAME = u"PySciDataGre group website"

# for developing (redefined in publishconf.py)
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'fr'

THEME = 'theme'
STATIC_PATHS = ['images', 'docs','files']

DIRECT_TEMPLATES = ('index', 'categories', 'authors', 'archives',
                    'publications')

# Plugins
PLUGIN_PATHS = ['plugins']
PLUGINS = []

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Python.org', 'http://python.org/'),
         ('AFPY', 'https://www.afpy.org/'),
         ('UGA', 'https://www.univ-grenoble-alpes.fr/'),
         ('CNRS', 'http://www.cnrs.fr'))

# Social widget
SOCIAL = (#('You can add links in your config file', '#'),
          #('Another social link', '#'),)
)

DEFAULT_PAGINATION = False

RELATIVE_URLS = True

#TAGS_SAVE_AS = ''
#TAG_SAVE_AS = ''

PUBLICATIONS_SRC = 'content/pubs.bib'

READERS = {'html': None}
IGNORE_FILES = ['*~']
ARTICLE_EXCLUDES = ['docs/ipynbslides']
