Working sessions
================

:date: 2018-06-03
:modified: 2018-06-03
:tags: Python
:category: Working

Join our Python working sessions in Grenoble.

Each month, we organize one working session of 2 hours.

Please find the guidelines for these sessions:

- Everyone is welcome (beginners to advanced users).

- Presentations/tutorials will last 30 to 60 min, on some Python topics that
  can be useful to many people, with practical examples.

- We want to have a lightning talk (5 min) after the main session so that
  people can quickly present how they use Python in their work (or a specific
  package, etc.).

- If you wish to share your Python experience during a working session and/or
  to co-animate a working session, please contact us.
