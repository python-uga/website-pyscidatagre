Links
=====

:date: 2017-12-15
:modified: 2017-12-15

Institutions
------------

- `Université Grenoble Alpes <http://www.univ-grenoble-alpes.fr/>`_

Open-source
-----------

- `Python.org <http://python.org>`_, `Scipy <http://www.scipy.org>`_, `Cython
  <http://cython.org/>`_, `Pythran
  <https://github.com/serge-sans-paille/pythran>`_, ...

- `The Free Software Foundation (FSF) <http://www.fsf.org>`_ and `GNU
  <https://www.gnu.org>`_

- `Debian <https://www.debian.org/>`_ and `Ubuntu <https://www.ubuntu.com/>`_
  
- `Pelican <http://getpelican.com/>`_ (the program used to make this site)
