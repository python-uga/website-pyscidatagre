Training
========

:date: 2017-12-15
:modified: 2017-12-15

We organize training sessions...

- Introduction to Python (`repository
  <https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017>`__
  and `presentations <https://python-uga.gricad-pages.univ-grenoble-alpes.fr/py-training-2017/>`__)

- Python for HPC (`repository
  <https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc>`__
  and `presentations <https://python-uga.gricad-pages.univ-grenoble-alpes.fr/training-hpc/>`__)
