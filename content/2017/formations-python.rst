Formations Python à l'UGA et ailleurs
=====================================

:date: 2017-12-15
:modified: 2017-12-15
:tags: Python
:category: Training

C'est parti pour 3 jours (7-9 nov. 2017) de formation sur les bases de Python à
l'UGA.  En fait ce sera à l'Irstea Grenoble, mais dans le cadre de la formation
continue de l'UGA. On attend une trentaine de personnes et on sera 4 formateurs
: Cyrille Bonamy (LEGI), Eric Maldonado (Irstea), Franck Thollard (ISTERRE) et
Pierre Augier (LEGI).

Les documents de formation sont dans ce dépôt:
https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017

J'en profite pour faire de la pub pour deux autres formations :

- `Python avancé pour le calcul scientifique
  <http://lyoncalcul.univ-lyon1.fr/events/2017/python/>`_ (à Lyon) qui tombe
  pile en même temps que notre formation...

- `Parallel and GPU programming in Python
  <https://events.prace-ri.eu/event/656/overview>`_ organisée dans le cadre de
  `PRACE (Partnership for Advanced Computing in Europe)
  <http://www.prace-ri.eu/>`_ à Amsterdam du 6 au 8 décembre 2017.

C'est intéressant de voir que Python devient un des langages importants du
calcul scientifique et même du HPC! A mettre en parallèle aussi avec cette
analyse sur le blog de stackoverflow : `The Incredible Growth of Python
<https://stackoverflow.blog/2017/09/06/incredible-growth-python/>`_.
