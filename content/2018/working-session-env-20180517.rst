Working session Bonnes pratiques et environnements Python
=========================================================

:date: 2018-05-17
:modified: 2018-05-17
:category: Workshops

Nous vous proposons une première working session du groupe Python le jeudi 17 mai
à 14h à l'auditorium du bâtiment IMAG.
https://www.openstreetmap.org/?mlat=45.19056&mlon=5.76728#map=19/45.19056/5.76728

Seront abordés:

- les PEPx les + importants

- les environnements de paquets

- quelques environnements de développement (Spyder, Jupyter, Vim, PyCharm, Visual
  Studio Code, Emacs, Pydev)

3 interventions sont prévues autour des bonnes pratiques et environnements
développement Python :

- Tools for python coding (Raphaël Bacher) `slides <https://bacherap.gricad-pages.univ-grenoble-alpes.fr/python-formation-ide/presentation.html>`__ 

- Jetbrains Python IDEs & Code Hinting (Damien Albert)

- Retour d'expérience sur l'utilisation de Vim comme IDE Python (Franck Thollard) `slides <files/slides_slidy.html>`__
