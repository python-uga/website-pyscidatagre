Journée Kickoff PySciDataGre
============================

:date: 2018-03-08
:modified: 2018-03-20
:tags: Python
:category: Conferences

Le groupe "PySciDataGre", Python scientifique et
données de Grenoble est officiellement lancé. 
Pour l'occasion,  une journée de conférence a été organisée où sont intervenus des
représentants alpins d'un large panel de champs disciplinaires sur ce qui se fait
en Python en sciences et en analyse de données. Deux "guest stars" de la communauté Python ont également participé
(à trouver dans la liste des interventions plus bas).

Les vidéos sont ici :
https://gricad.univ-grenoble-alpes.fr/multimedia/videos/journee-python

Lieu : Batiment IMAG 700 avenue Centrale Saint Martin d'hères

Programme
~~~~~~~~~

- 09:00 Accueil Café

- 09:30 Introduction (Eric Maldonado, OSUG-Irstea)  `slides <files/slides_python_8mars.pdf>`__

- 09:50 How to build a successful scientific software package: the scikit-learn
  model (Alexandre Gramfort, INRIA) `slides <files/scikit_learn_success_gramfort.pdf>`__

- 10:40 Transition de Matlab à Python - quelques exemples simples (Brigitte
  Bidegaray-Fesquet, LJK) `slides <files/expose_Bidegaray.pdf>`__


- 11:00 Visualisez un milliard (et au-delà) d évènements en Python : de l idée au
  prototype interactif (Renaud Blanch, LIG) `slides <files/2018_03_08_Blanch_BiVis.pdf>`__

- 11:25 Jupyter (Raphael Bacher, Data Institute) `slides <https://github.com/raphbacher/presentation-jupyter-08-03-2018/blob/master/presentation_jupyter.ipynb>`__

- 12:00 Déjeuner

- 13:30 Optimizing the core of Python Scientific Stack (Serge Guelton, Namek -
  Quarkslab) `slides <http://serge-sans-paille.github.io/talks/python-grenoble-2018/out/index.html#/step-1>`__

- 14:20 Synchrotron and optics Imaging of biological materials (Aurélien Gourrier,
  LiPHY) slides

- 14:45 Traitements SIG appliqués à la modélisation spatiale des domaines skiables
  (Hugues François, Irstea) `slides <files/Pysciedatagre_20180208_HFrancois.pptx>`__

- 15:10 Yade, a C++/Python code for Numerical modeling of granular and multiphase
  systems (Bruno Chareyre, 3SR) `slides <files/journee_python_2018sanitized.pdf>`__

- 15:35 Pause café

- 15:55 Laboratory experiments in the Coriolis platform with Python (Pierre
  Augier, LEGI) `slides <http://www.legi.grenoble-inp.fr/people/Pierre.Augier/docs/ipynbslides/201803pygre/pres201803pygre.slides.html#/>`__

- 16:20 Chasing exoplanets with Python and machine learning (Carlos Gomez, IPAG,
  Data Institute) `slides <https://speakerdeck.com/carlgogo/chasing-exoplanets-with-python-and-machine-learning>`__

- 16:45 Conclusion
