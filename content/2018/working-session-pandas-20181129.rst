Working session Librairie Pandas 
================================

:date: 2018-11-29
:modified: 2018-11-29
:category: Workshops

Nous vous proposons une troisième working session du groupe Python le jeudi 29 novembre 
à 14h à l'auditorim du batiment IMAG (accueil à 13h30 pour le café)
https://www.openstreetmap.org/?mlat=45.19056&mlon=5.76728#map=19/45.19056/5.76728

`Franck Iutzeler <http://www.iutzeler.org/>`__ présentera une introduction à la librairie Pandas.
Les supports sont accessibles `ici <https://github.com/iutzeler/Pres_Pandas/>`__

La solution des exercices `ici <http://www.iutzeler.org/docs_teach/Python_SSD/SOL_3-2_Dataframes.ipynb>`__  

**Mots clés** : Pandas, Numpy, Dataframes, Data Sciences 
