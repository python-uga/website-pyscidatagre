Working session Traitement d'images avec Python et Spyder
=========================================================

:date: 2018-06-21
:modified: 2018-06-21
:category: Workshops

Nous vous proposons une seconde working session du groupe Python le jeudi 21 juin
à 14h à Irstea, salle Ecrins (accueil à 13h30 pour le café)
https://www.openstreetmap.org/query?lat=45.19945&lon=5.77291

`Patricia Ladret <http://www.gipsa-lab.grenoble-inp.fr/page_pro?vid=101>`__
présentera comment passer facilement de Matlab à Python pour le traitement du
signal et des images.  Les ressources sont accessibles.

`ici <http://chamilo.univ-grenoble-alpes.fr/main/lp/lp_controller.php?cidReq=UGA000028&id_session=0&gidReq=0&gradebook=0&origin=&id_session=0>`__

On utilisera Spyder et pas mal de librairies dédiées au traitement du signal, des images et des vidéos.

La présentation sera accessible aux non-spécialistes de l'image et au débutant en
Python!

**Mots clés** : Scipy, scikit-images, Pyav, opencv-python, Spyder
