Formations Python CED UGA
=========================

:date: 2018-04-25
:modified: 2018-04-25
:tags: Python
:category: Training

C'est parti pour 3 jours (25-27 avril 2018) de `formation sur les bases de Python
<https://www.adum.fr//script/formations.pl?menu_transparent=oui&mod=162721&site=CDUDG>`_
organisée par le CED à l'UGA.  On attend une grosse dizaine de personnes et on va
tourner avec 4 formateurs : Cyrille Bonamy (LEGI), Eric Maldonado (Irstea), Franck
Thollard (ISTERRE) et moi.

Les documents de formation sont dans ce dépôt:
https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017
