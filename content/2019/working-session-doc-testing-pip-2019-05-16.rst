Working session Documentation and testing Environment 
=====================================================

:date: 2019-05-16
:modified: 2019-05-16
:category: Workshops

Nous vous proposons une sixième working session du groupe Python le jeudi 16 mai à 10h à Irstea, salle Ecrins (accueil à 9h30 pour le café)
http://www.openstreetmap.org/#map=18/45.19935/5.77285

Rémi and Edward will present the documentation and testing environment around the "Software for Practical Analysis of Materials".
It is based on a workflow on gitlab -- we currently use the one provided by Gricad.
`slides reveal.js <files/2019-05-16-SPAMdocAndTest.html>`__
**Mots clés** : Documentation, Testing, pip, gitlab




