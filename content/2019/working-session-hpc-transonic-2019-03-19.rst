Working session Python HPC landscape and zoom on Transonic 
==========================================================

:date: 2019-03-19
:modified: 2019-03-19
:category: Workshops

Nous vous proposons une cinquième working session du groupe Python le mardi 19 mars 
à 14h à l'auditorium du batiment IMAG (accueil à 13h30 pour le café)
https://www.openstreetmap.org/?mlat=45.19056&mlon=5.76728#map=19/45.19056/5.76728

Pierre Augier présentera un panorama du calcul intensif sous Python et fera un focus sur `transonic <https://bitbucket.org/fluiddyn/transonic>`__ pour que code s'envole à la vitesse transsonique `slides <http://www.legi.grenoble-inp.fr/people/Pierre.Augier/docs/ipynbslides/20190319_PySciDataGre_transonic/pres_20190319_PySciDataGre_transonic.slides.html>`__
**Mots clés** : HPC, numpy, transonic




