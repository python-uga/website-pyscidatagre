Working session HDF5/h5py 
============================================

:date: 2019-11-21
:modified: 2019-11-21
:category: Workshops

Nous vous proposons une 8ème working session du groupe Python jeudi 21 novembre à 10h au LEGI dans l'amphi K118 (accueil à 9h30 pour le café)

Loïc Huder (Isterre) présentera : Le format de fichier HDF5 avec h5py: un cas d'utilisation en assimilation et visualisation de données géomagnétiques

Résumé:

Python est maintenant un langage très utilisé dans les domaines scientifiques principalement grâce aux bibliothèques NumPy et SciPy. A cet écosystème Python scientifique vient se greffer la bibliothèque h5py qui interface le format de fichier HDF5 en Python. Ce format de fichier permet de stocker de manière efficace et hiérarchique des données.

Le but de cette présentation est de montrer que le format de fichier HDF5 et l'interfaçage de h5py avec NumPy sont des outils très utiles pour le stockage organisé de données scientifiques et la recherche reproductible. Je ferai un exposé/tutoriel de l'utilisation de HDF5 à travers de h5py avant de présenter un cas d'utilisation concret dans les packages Python développés dans notre équipe: pygeodyn (assimilation de données géomagnétiques) et webgeodyn (visualisation de données géomagnétiques).

`slides <https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/working-sessions/-/blob/master/191121_HDF5_h5py/Huder_hdf5_h5py.pdf>`__


**Mots clés** : HDF5, h5py, NumPy 


