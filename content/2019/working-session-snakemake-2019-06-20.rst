Working session snakemake moteur de workflow 
============================================

:date: 2019-06-20
:modified: 2019-06-20
:category: Workshops

Nous vous proposons une 7ème working session du groupe Python jeudi 20 juin à 9h30 à l'auditorium de l'IMAG (accueil à 9h pour le café)

Florent Chuffart présentera le moteur de workflow snakemake sur les serveurs de calcul du mesocentre CIMENT/GRICAD.
Un cas pratique : traitement de données biologiques (RNA-seq, transcriptome).

Après un bref rappel des principes élémentaires du calcul parallèle nous détaillerons la mise en œuvre d’une tache simple de bio-informatique.
prérequis : ssh, bash.

**Mots clés** : automatisation, calcul, workflow 




