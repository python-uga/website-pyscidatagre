Working session Bonnes pratiques, documentation, packaging 
==========================================================

:date: 2019-02-12
:modified: 2019-02-12
:category: Workshops

Nous vous proposons une quatrième working session du groupe Python le mardi 12 février 
à 10h à l'auditorium du batiment IMAG (accueil à 9h30 pour le café)
https://www.openstreetmap.org/?mlat=45.19056&mlon=5.76728#map=19/45.19056/5.76728

Raphaël Bacher présentera des bonnes pratiques de développement en Python (documentation, packaging...). `slides <files/ws4_doc_packaging_bacher.pdf>`__
**Mots clés** : Documentation, bonnes pratiques, package
