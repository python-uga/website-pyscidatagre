Sources of the website of the PySciDataGre group
================================================

Get the source
--------------

Clone the repository (with `hg-git <http://hg-git.github.io/>`_)::

  hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/website-pyscidatagre.git

or (with ssh)::

  hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:python-uga/website-pyscidatagre.git

If you like the complicated git commands and that you understand the git index
:-) you can also replace ``hg`` by ``git``.

Get help to compile and serve the website
-----------------------------------------

The site is deployed with [pelican](http://docs.getpelican.com), so first
create a new python virtual environement:

   cd website-pyscidatagre/
   python -m venv venv
   source venv/bin/activate
   pip install -r requirements.txt

Then, edit the contents in `content`.

Finally, compile the html with

   make html

and open the file with a browser, for instance:

   firefox output/index.html


For help::

  make help


